import java.util.Random;
public class Wordle{
	//This method generates a random number between 0 and 19 and uses it to get an element from the list array, which it then returns under the string variable called "word".
	public static String generateWord(){
		Random random = new Random();
		String[] list = {"BUILD", "WRITE", "CRAZY", "PINKY", "BLOCK", "BLACK", "MONEY", "OLIVE", "SHARK", "STONE", "BROWN", "CLOWN", "SLICE", "ADULT", "CHILD", "EARLY", "BRUSH" , "BREAD", "TABLE", "GRAPE"};
		int i = random.nextInt(20); 
		String word = list[i];
		return word;
	}
	/*In this method, we take the word generated in the previous method, and the char c. 
	This method compares each character in the string "word" with the character c (char c). 
	If the characters match, the method returns true. 
	If all characters are checked and none of them match, the method returns false.
	*/
	public static boolean letterInWord(String word, char c){
		int i = 0;
		while(i < word.length()) {
			if (c == word.charAt(i)){
				return true;
			}
			i++;
		} return false;
	} 

	/*This method takes a string which represents the genrated word from the list, a character, and the position of the character in the string.
	It compares the character passed to the character at the specified position in the generated word. 
	If the characters match, the method returns true, otherwise it returns false.
	*/
	public static boolean letterInSlot(String word, char c, int position){
		
			if (c == word.charAt(position)){ 
				return true;
			} else { 
			return false;  
		}
	}
	/* This method takes the generated word, and the guess from the user. 
	It creates an array of string of length 5. 
	For each character, it calls the letterInSlot method. 
	*/
	public static String[] guessWord(String word, String guess){
		String[] colors = new String [5];
		for (int i = 0; i < guess.length(); i++){
			
			if(letterInSlot(word, guess.charAt(i), i)){
				colors[i] = "green";//If a char is in the correct position in a word, then the current index of the array "colors" will be assigned the value "green".
			} else if(letterInWord(word, guess.charAt(i))){
				colors[i] = "yellow"; //If a char isn't in a word but is present in a string, the current index of the array "colors" will be assigned the value "yellow".
			} else { 
				colors[i] = "white"; //if none of the conditions are met, the current index of the array "colors" will be assigned the value "white".
			}
		}
		return colors; //returns an array of colors
	}
	
	public static void presentResults(String word, String[] colors){
		for(int i = 0; i<word.length(); i++){
			if(colors[i].equals("green")){
				System.out.print("\u001B[32m"+ word.charAt(i));
			} else if(colors[i].equals("yellow")) {
				System.out.print("\u001B[33m"+ word.charAt(i));
			} else if(colors[i].equals("white")) {
				System.out.print("\u001B[0m"+ word.charAt(i));
		}
	} System.out.println(" ");
	
  }
	/* This method accepts input from the user in a variable named guessInput. 
	Then there is a condition that says that if the input length is not 5, 
	it should print text where it asks the user to enter a 5 letter input.
	Then, it converts lowercase input to uppercase.
	It return converted input.
	*/
	public static String readGuess(){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("Enter your guess: "); 
		String guessInput = reader.next();
		if (guessInput.length() != 5){
			System.out.println("You must enter a 5 letter word! Try again..");
			guessInput = reader.next();
		}
		String guess = guessInput.toUpperCase();
		return guess;
	}
	
	public static void runGame(String word){ 
		//This loop that checks that the user has had no more than 6 attempts
		for(int i = 0; i < 6; i++){
		//Calls the readGuess method, storing its result in the guess variable
		String guess = readGuess();
		//Calls the guessWord method, storing its result in array results
		String[] results = guessWord(word, guess);
		//Calls the presentResults method, in order to print the previous array using colors. 
		presentResults(guess, results);
		//if the generated word is equal to the user's input, it will print the text with congratulations 
		if(word.toUpperCase().equals(guess.toUpperCase())) {
			System.out.println("Good job, you win!");	
			return; //once return is executed, the rest of the methods ends
			}	 
		} 
		System.out.println("Bad luck, try again..."); //otherwise, it will print this
	}

}
