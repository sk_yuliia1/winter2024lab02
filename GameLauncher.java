public class GameLauncher {
	public static void main(String[] args){
		launchGame();
	}
	/* This method takes no parameter and returns nothing. 
		It greets the user and asks him what game he wants to run.
		In the while loop, it compares the user's input using different 'if' statements in order to envoke the needed method(game)
		If user entered 'q', the program will stop. 
	*/
	public static void launchGame(){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("~~Welcome to the Word Games launcher~~");
		while(true){
			System.out.println("~~To run Hangman enter 'hang' / To run Wordle enter 'wordle'~~");
			System.out.println("Enter 'q' to exit");
			String choice = reader.next();
			if(choice.equals("hang")){
				runHangmanGame();	
			} else if(choice.equals("wordle")){
				runWordleGame();
			}else if(choice.equals("q")){
				break;
			} else{ 
			System.out.println("Error: wrong input; Try again");
			}
		}
	}
	/*
	This method asks the user to enter an input, a string that represents the word user will try to guess. 
	If the input is more than 4 characters long, the program will end.
	It calls method runGame from the class Hangman.
	*/
	public static void runHangmanGame(){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("NOTE: You have a maximum of 6 attempts to guess");
		System.out.println("Enter a 4 letter word: ");
		String word = reader.next();
		if (word.length()>4){
			System.out.println("You must enter 4 letter word!");
		} else {
			Hangman.runGame(word);
		}
	}
	/*
	This method runs method runGame from the class Wordle, which takes as a parameter a result of generateWord method from the class Wordle 
	*/
	public static void runWordleGame(){
		Wordle.runGame(Wordle.generateWord());
		
	}
	
}